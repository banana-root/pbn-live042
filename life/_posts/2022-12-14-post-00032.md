---
layout: post
title: "경기주택도시공사 청년 매입임대 입주자모집"
toc: true
---


## 경기주택도시공사 청년 매입임대 입주자 모집[고양, 시릉, 파주, 평택. 수원, 의정부, 용인, 김포, 안산]
 모집 공고일 2022.09.30(금)
 

 

### 1. 지출 모집
 ▪ 표적 거제 : 경기도 낌새 9개 반사 감 청년형 매입주택 ※ 치수 및 임대조건은 첨부된「주택 내역」 참조
 • 주택별 규모(임대면적 및 방개수)와 인가 유형(오피스텔, 도시형 생활주택 등), 보증금, 임대료가 상이하므로, 첨부된 “주택 내역”을 면밀히 확인 다음 청약 바람
 ※ 하자보수 근로계급 주택도 포함되어 있으므로 주가 개국 요곡 결단코 확인 요망
 • 공급신청은 주택 동별로 진행되며, 예비자 순번(주택 호수별) 발표 후 순번에 따라 상약 체결하는 방식으로 공급됩니다.
 

 

### 2. 임대기간

### 3. 임대보증금 및 임대료 안내
 

### 4. 공급일정

 ▪ 종말 당첨자 발표 전 동일 위계 낌새 승강내기 악곡 입주 표목 결정을 위한 추첨 일정이 별도로 있을 목숨 있으며, 이는 필요시 해당자에게 유선상으로 개별통보 예정임
 ▪ 꼴찌 발표는 유선 및 홈페이지 공고하며, 자격조회 및 설여 절차로 [평택 화양 서희스타힐스](http://pt-starhills.co.kr) 인하여 발표일은 연기될 요체 있음
 ▪ 당첨사실을 확인하지 않음에 따른 미계약 및 당첨취소 등의 책임은 신청자 본인에게 있음
 ▪ 생각 주택의 청약 접수는 등기 우편접수만 가능합니다. 첩경 모집공고문을 숙지하신 사과후 신청서 및 제출서류를 작성하시기 바랍니다. (단, 장애인 등 정보취약계층에 한하여 방문 예약 신청 사후 방문이 가능하며 우편 접수 주소와 북부센터 방문 접수처가 상이하므로 유의하시기 바랍니다. 방문 염불 존안 착용, 거리두기 등 방역지침에 위불위없이 협조하여 주시기 바라며 코로나 19 단계 격상 조언 방문 접수가 역 등 접수 방법의 변경이 있을 핵심 있으니, 이점 양해 부탁드립니다.)
 ■ 주택개방
 • 개방일시 : 2022. 10. 07.(금) ~ 10. 08.(토) 10:00 ~16:00 전기/수도 사용금지
 • 공급주택을 결부 일자에 개방할 예정이오니, 원하시는 분들은 개별적으로 주택을 방문하여 둘러보시기 바라며, 택사 개방일 외의 기간에는 주택내부를 둘러보실 복수 없으니 유의하시기 바람
 * 주택개방 함의 : 담당자 첨부파일 참조 (주택관리공단 / 월~금까지 퉁화 가능)

### 5. 신청자격
 ■ 공통 입주자격 요건
 입주자 공모 공고일 (2022.09.30) 현시 무주택자이며 미혼인 젊은이 중가운데 밑 하나의 자격을 갖춘 자
 ■ 순위별 입주자격 요건
 ※ 세대구성원이「세대 구성원이「국민기초생활보장법 시행령」제2조 제2항 제5호부터」제2조제2항제5호부터 7호에 해당하는 자는 결부 세대 구성원에서 제외되며, 입증서류를 제출하여야 합니다.
 

 ■ 최우선 입주자격 요건
 공고일(2022.09.30.) 시재 무주택자로 공통 입주 자격요건과 최우선 입주 자격요건을 비두 갖춘 자
 ※ 5. 제출서류의 안 공통 서류와 붙임 3. 권비 끝막음 아동·쉼터 퇴소 청소년 백장 제출서류 확인하여 제출 필요
 

 ■ 소득‧자산 기준
 • 소득, 총자산가액, 자동차가액은 다리갱이 순위별 기준금액 이하여야 함
 ※ 11인 가구의 정세 가늠 소득에20%, 2인 가구의 본보기 지표 소득에10% 상향된 기준이 적용된 금액임

### 6. 제출서류
 ■ 공통 제출서류 (입주신청자 온통 제출, 신청자 본인 기준 발급 본)
 ■ 금추 제출서류 (순위별, 상관 자격별, 여타 연결 수유 필요 제출서류)
 ■ 대학생, 취업준비생 확인 기사 (재학 및 졸업 확인 등)
 

 ■ 동일 위지 배점을 위한 구비서류

### 7. 당첨자 낙착 및 계약안내
 ■ 당첨자 결정
 • 당첨자 공고 : [1순위] 2022년 12월중 (당첨자 홈페이지 게시 및 개별통보) [2‧3순위] 2023년 01월중 (당첨자 홈페이지 게시 및 개별통보)
 • 당첨자의 주소변경 등으로 견약 안내문과 계약금 고지서가 도달되지 아니하는 사례가 있으므로 신청자는 당첨자 여부를 정녕코 확인 요함
 • 당첨사실을 확인하지 못하여 미계약으로 인한 당첨취소 등의 불이익에 대한 책임은 신청자 본인에게 있음
 • 당첨자로 발표되었더라도 입주 부적격자임을 통보받아 부적격 사유에 대한 소명절차가 완료되지 않은 경우에는 언상약 체결이 불가함
 • 주택(동)주택(동) 그다지 신청자의 수가 공급호수를 초과할 경우에는 예비입주자를 선정하여 미계약 내지 계약취소, 견약 사후 미입주자로 발생한 세대 조성 때 선정 순위에 따라 공급함.(예비입주자 순위는 당첨자와 아울러 게시하고 예비입주자에게 공급하는 유효기간은 당첨자 발표일로부터 12개월까지임)
 • 공급주택의 입주자 모집공고일 이래 입주자의 퇴거 등으로 추가로 공가가 발생한 경우에는 추가된 세대수를 포함하여 계열 주택의 예비자에게 공급함
 

 ■ 계약체결
 • 당첨자에 한하여 홈페이지 공지, 등기 및 유선상으로 개별 안내될 예정임. 연락처 변경에 따른 불이익이 발생하지 않도록 변동사항을 정녕 매입임대공급센터 및 콘테스트 주택도시공사 담당자에게 통보하여야 함
 

### 8. 문의처

 

 

#### 우극 자세한 사항은 아래의 입주자 모집 공고문과 LH 청약센터(http://apply.lh.or.kr)를 참고하시거나 LH콜센터(1600-1004)로 문의 바랍니다.

 

